module.exports = string_4;


function string_4(obj) {
    if (obj == undefined || typeof obj != "object") {
        return 0;
    }
    let name = "";
    if (obj.hasOwnProperty('first_name') || obj.hasOwnProperty('last_name')) {
        if (obj.hasOwnProperty('middle_name')) {
            name = obj.first_name[0].toUpperCase() + obj.first_name.slice(1).toLowerCase() + " ";
            name += obj.middle_name[0].toUpperCase() + obj.middle_name.slice(1).toLowerCase() + " ";
            name += obj.last_name[0].toUpperCase() + obj.last_name.slice(1).toLowerCase() ;
        }
        else {
            name = obj.first_name[0].toUpperCase() + obj.first_name.slice(1).toLowerCase() + " ";
            name += obj.last_name[0].toUpperCase() + obj.last_name.slice(1).toLowerCase() ;
        }
        return name;
    }
   
}


