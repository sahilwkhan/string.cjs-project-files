// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

const string_problem_3 = require('./string3.cjs')

result = string_problem_3("10/5/2021");

console.log(result);