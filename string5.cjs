module.exports = string_5;


function string_5(arr) {
    if (arr == undefined || typeof arr != "object") {
        return [];
    }
    let str = "";
    for (let element = 0; element < arr.length; element++){
        if (element == arr.length-1) {
            str += arr[element] + ".";
        }
        else {
            str += arr[element] + " ";
        }
    }
    return str;
}


