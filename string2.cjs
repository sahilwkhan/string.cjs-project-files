module.exports = string_2;


function string_2(str) {
    if (str == undefined || typeof str != "string") {
        return [];
    }
    
    ip_array = []
    part = ""
    count = 0;
    for (let i = 0; i < str.length; i++){
        
        if ((str.charCodeAt(i) >= 48 && str.charCodeAt(i) <= 57) || str[i] == ".") {
            if (str[i] == ".") {
                count = 0;
            }
            else {
                if (str.charCodeAt(i) != 47) {
                    count++;
                    part += str[i];
                    if (count == 3) {
                        ip_array.push(parseInt(part));
                        part = "";
                    }
                }
            }
        }
        else{
            return [];
        }
    }
    return ip_array;
}

