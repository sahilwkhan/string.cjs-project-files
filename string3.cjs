module.exports = string_3;


function string_3(str) {
    if (str == undefined || typeof str != "string") {
        return 0;
    }
    month_array = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    run = 0;
    for (let i = 0; run == 0; i++){
        month = "";
        if (str[i] == "/") {
            run = 1;
            for (let j = i+1; str[j] != "/"; j++) { 
                month += str[j];
            }
            month = parseInt(month);
            return month_array[month-1];
        }
    }
}


