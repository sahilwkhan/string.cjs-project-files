module.exports = string_1;


function string_1(str) {
    if (str == undefined || typeof str != "string") {
        return 'Only string datatype accepted.';
    }
    new_str = ''
    for (let i = 0; i < str.length; i++){
        if ((str.charCodeAt(i) >= 48 && str.charCodeAt(i) <= 57) || str[i] == '-' || str[i] == ".") {
            if (str[i] == "-" && i >1) {
                return 0;
            }
            if (str.charCodeAt(i) != 47) {
                new_str += str[i];
            }

        }
        else if (str[i] == '$') {
            if (i > 1) {
                return 0;
            }
        }
        else{
            if (i != 0 && str[i] != ",") {
                return 0;
            }
        }
    }
    return new_str;
}

