// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

const string_problem_5 = require('./string5.cjs')

result = string_problem_5(["the", "quick", "brown", "fox"]);

console.log(result);
