// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

const string_problem_4 = require('./string4.cjs')

result = string_problem_4({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"});

console.log(result);
